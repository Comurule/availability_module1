const router  = require('express').Router();

const {
    department_create_get, department_create_post,
    department_delete_get, department_delete_post,
    department_details_get, department_list_get,
    department_update_get, department_update_post
} = require ('../controllers/department');

router.get('/create', department_create_get);
router.post('/create', department_create_post);

router.get('/:department_id/update', department_update_get);
router.post('/:department_id/update', department_update_post);

router.get('/:department_id/delete', department_delete_get);
router.post('/:department_id/delete', department_delete_post);

router.get('/:department_id', department_details_get);
router.get('/', department_list_get);

module.exports = router;