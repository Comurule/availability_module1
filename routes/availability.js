const router  = require('express').Router();

const {
    availability_create_get, availability_create_post,
    availability_delete_get, availability_delete_post,
    availability_details_get, availability_list_get,
    availability_update_get, availability_update_post
} = require ('../controllers/availability');

router.get('/create', availability_create_get);
router.post('/create', availability_create_post);

router.get('/:availability_id/update', availability_update_get);
router.post('/:availability_id/update', availability_update_post);

router.get('/:availability_id/delete', availability_delete_get);
router.post('/:availability_id/delete', availability_delete_post);

router.get('/:availability_id', availability_details_get);
router.get('/', availability_list_get);

module.exports = router;