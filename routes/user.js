const router  = require('express').Router();
const {  
    user_create_get, user_create_post,
    user_delete_get, user_delete_post,
    user_details_get, user_list_get,
    user_login_get, user_login_post,
    user_update_get, user_update_post
} = require ('../controllers/user');

router.get('/create', user_create_get);
router.post('/create', user_create_post);

router.get('/:user_id/update', user_update_get);
router.post('/:user_id/update', user_update_post);

router.get('/:user_id/delete', user_delete_get);
router.post('/:user_id/delete', user_delete_post);

router.get('/login', user_login_get);
router.post('/login', user_login_post);

router.get('/:user_id', user_details_get);
router.get('/', user_list_get);


module.exports = router;
