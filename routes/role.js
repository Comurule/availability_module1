const router  = require('express').Router();

const {
    role_create_get, role_create_post,
    role_delete_get, role_delete_post,
    role_details_get, role_list_get,
    role_update_get, role_update_post
} = require ('../controllers/role');

router.get('/create', role_create_get);
router.post('/create', role_create_post);

router.get('/:role_id/update', role_update_get);
router.post('/:role_id/update', role_update_post);

router.get('/:role_id/delete', role_delete_get);
router.post('/:role_id/delete', role_delete_post);

router.get('/:role_id', role_details_get);
router.get('/', role_list_get);

module.exports = router;