const router  = require('express').Router();

const userRoute = require ('../routes/user');
const roleRoute = require ('../routes/role');
const departmentRoute = require ('../routes/department');
const availabilityRoute = require ('../routes/availability');
const current_businessRoute = require ('../routes/current_business');

//User Routes
router.use('/users', userRoute);

//Role Routes
router.use('/roles', roleRoute);

//Department Routes
router.use('/departments', departmentRoute);

//Availability Routes
router.use('/availabilities', availabilityRoute);

//Current_business Routes
router.use('/current_business', current_businessRoute);

//Index Route
router.get('/', (req, res) => { res.status(301).send('Welcome to our Availability module project.') })


module.exports = router;
