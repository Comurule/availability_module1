const router  = require('express').Router();

const {
    current_business_create_get, current_business_create_post,
    current_business_delete_get, current_business_delete_post,
    current_business_details_get, current_business_list_get,
    current_business_update_get, current_business_update_post
} = require ('../controllers/current_business');

router.get('/create', current_business_create_get);
router.post('/create', current_business_create_post);

router.get('/:current_business_id/update', current_business_update_get);
router.post('/:current_business_id/update', current_business_update_post);

router.get('/:current_business_id/delete', current_business_delete_get);
router.post('/:current_business_id/delete', current_business_delete_post);

router.get('/:current_business_id', current_business_details_get);
router.get('/', current_business_list_get);

module.exports = router;